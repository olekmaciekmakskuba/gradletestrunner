Docelowo program ma przyjąć za pomocą serwera RESTowego listę plików, które następnie należy przekompilować, uruchomić testy i uaktualnić bazę danych. Content plików przesyłany jest w base64.

Wymagania działania aplikacji:

* JDK8
* gradle 

Aktualne przykładowe zapytanie REST (będzie ulegało zmianie):

* metoda: POST
* url: localhost:8080/submission
* Content-Type: application/json
* request body: 

```
#!json

{"classes":[
{
"packageName": "sample",
"fileName": "Math.java",
"content": "cGFja2FnZSBzYW1wbGU7DQoNCnB1YmxpYyBjbGFzcyBNYXRoIHsNCgkNCglwdWJsaWMgZG91YmxlIGFkZChkb3VibGUgZmlyc3QsIGRvdWJsZSBzZWNvbmQpewkJDQoJCXJldHVybiBmaXJzdCtzZWNvbmQ7DQoJfQ0KfQ=="
}],
"tests":[
{
"packageName": "sample",
"fileName": "MathTest.java",
"content": "cGFja2FnZSBzYW1wbGU7DQoNCmltcG9ydCBvcmcuanVuaXQuVGVzdDsNCg0KaW1wb3J0IHN0YXRpYyBvcmcuanVuaXQuQXNzZXJ0Lio7DQoNCnB1YmxpYyBjbGFzcyBNYXRoVGVzdCB7DQoNCglwcml2YXRlIE1hdGggbWF0aCA9IG5ldyBNYXRoKCk7DQoJDQoJQFRlc3QNCglwdWJsaWMgdm9pZCBzaG91bGRBZGROdW1iZXJzKCl7DQoJCS8vIGdpdmVuDQoJCWludCBhID0gMTA7DQoJCWRvdWJsZSBiID0gMi41Ow0KCQkNCgkJLy8gd2hlbg0KCQlkb3VibGUgcmVzdWx0ID0gbWF0aC5hZGQoYSwgYik7DQoJCQ0KCQkvLyB0aGVuDQoJCWFzc2VydEVxdWFscyhyZXN1bHQsIDEyLjUsIDAuMDAxKTsJCQ0KCX0JDQoJDQoJQFRlc3QNCglwdWJsaWMgdm9pZCB0aGlzVGVzdFNob3VsZEZhaWxlZCgpew0KCQlhc3NlcnRGYWxzZSgiYmxhYmxhIix0cnVlKTsNCgl9DQp9DQo="
}],
"buildGradle": 
{
"content": "YXBwbHkgcGx1Z2luOiAnamF2YScNCg0Kc291cmNlQ29tcGF0aWJpbGl0eSA9IDEuNw0KdmVyc2lvbiA9ICcxLjAnDQoNCnJlcG9zaXRvcmllcyB7DQogICAgbWF2ZW5DZW50cmFsKCkNCn0NCg0KZGVwZW5kZW5jaWVzIHsNCg0KICAgIHRlc3RDb21waWxlIGdyb3VwOiAnanVuaXQnLCBuYW1lOiAnanVuaXQnLCB2ZXJzaW9uOiAnNC4xMicNCn0="
},
"submissionId": 110
}
```