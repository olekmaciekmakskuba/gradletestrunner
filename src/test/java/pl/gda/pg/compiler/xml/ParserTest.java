package pl.gda.pg.compiler.xml;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pl.gda.pg.compiler.spring.SpringConfiguration;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringConfiguration.class)
public class ParserTest {

	@Inject
	private Parser testObj;

	@Test
	public void shouldParseXml() throws URISyntaxException {
		//given
		URI testFile = Thread.currentThread().getContextClassLoader().getResource("test.xml").toURI();
		Path path = Paths.get(testFile);

		//when
		TestSuiteXml testSuiteXml = testObj.parse(path);

		//then
		List<TestCaseXml> testCases = testSuiteXml.getTestCases();

		int testCasesSize = testCases.size();
		assertThat(testCasesSize, is(2));

		TestCaseXml firstTestCase = testCases.get(0);
		assertTrue(firstTestCase.isPassed());

		String firstMessage = firstTestCase.getMessage();
		assertThat(firstMessage, is(""));

		TestCaseXml secondTestCase = testCases.get(1);
		assertFalse(secondTestCase.isPassed());

		String secondMessage = secondTestCase.getMessage();
		assertThat(secondMessage, is("java.lang.AssertionError: blabla"));
	}
}