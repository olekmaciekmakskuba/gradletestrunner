package pl.gda.pg.compiler.spring;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class CompilerEntryPoint {

	public static void main(String... args) {
		new AnnotationConfigApplicationContext(SpringConfiguration.class);
	}
}
