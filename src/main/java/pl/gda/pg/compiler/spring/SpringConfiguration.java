package pl.gda.pg.compiler.spring;

import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import pl.gda.pg.commons.vertx.VertxWrapper;
import pl.gda.pg.compiler.xml.TestSuiteXml;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Configuration
@ComponentScan("pl.gda.pg.compiler.*")
public class SpringConfiguration {

	@Bean
	public VertxWrapper getEventBusWrapper() throws ExecutionException, InterruptedException {
		CompletableFuture<Vertx> vertxFuture = getVertxAsync();
		Vertx vertx = vertxFuture.get();

		return new VertxWrapper(vertx);
	}

	@Bean
	public Jaxb2Marshaller jaxb2Marshaller() {
		Jaxb2Marshaller jaxb = new Jaxb2Marshaller();
		jaxb.setClassesToBeBound(TestSuiteXml.class);
		return jaxb;
	}

	private CompletableFuture<Vertx> getVertxAsync() {
		CompletableFuture<Vertx> future = new CompletableFuture<>();

		VertxOptions options = new VertxOptions();
		options.setClusterHost("localhost");
		Vertx.clusteredVertx(options, event -> future.complete(event.result()));

		return future;
	}
}
