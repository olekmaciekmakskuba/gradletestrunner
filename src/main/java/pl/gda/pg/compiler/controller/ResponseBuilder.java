package pl.gda.pg.compiler.controller;

import pl.gda.pg.compiler.structure.ProjectPaths;
import pl.gda.pg.compiler.xml.Parser;
import pl.gda.pg.compiler.xml.TestCaseXml;
import pl.gda.pg.compiler.xml.TestSuiteXml;
import pl.gda.pg.shared.data.SubmissionDetail;
import pl.gda.pg.shared.data.SubmissionResponse;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Named
public class ResponseBuilder {

	@Inject
	private Parser parser;
	@Inject
	private ProjectPaths paths;

	public SubmissionResponse build(int submissionId) {
		SubmissionResponse response = new SubmissionResponse();

		Path testsPath = paths.getProjectTests(submissionId);

		createDetails(testsPath).ifPresent(response::setDetails);
		response.setId(submissionId);
		return response;
	}

	private Optional<List<SubmissionDetail>> createDetails(Path testsPath) {
		try {
			Stream<Path> list = Files.list(testsPath);
			List<SubmissionDetail> details = list.filter(this::filterPaths)
												 .map(this::parseTest)
												 .flatMap(this::mapToDetailsStream)
												 .collect(Collectors.toList());
			return Optional.of(details);
		} catch (IOException e) {
			e.printStackTrace();
			return Optional.empty();
		}
	}

	private boolean filterPaths(Path p) {
		Path fileName = p.getFileName();
		return fileName.toString().endsWith(".xml");
	}

	private Stream<SubmissionDetail> mapToDetailsStream(TestSuiteXml testSuiteXml) {
		return testSuiteXml.getTestCases()
						   .stream()
						   .map(this::mapToDetail);
	}

	private SubmissionDetail mapToDetail(TestCaseXml testCaseXml) {
		SubmissionDetail detail = new SubmissionDetail();
		detail.setTestName(testCaseXml.getName());
		detail.setPassed(testCaseXml.isPassed());
		detail.setMessage(testCaseXml.getMessage());

		return detail;
	}

	private TestSuiteXml parseTest(Path path) {
		return parser.parse(path);
	}
}
