package pl.gda.pg.compiler.controller;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import pl.gda.pg.commons.vertx.VertxWrapper;
import pl.gda.pg.compiler.runner.GradleRunner;
import pl.gda.pg.compiler.structure.ProjectFilesCreator;
import pl.gda.pg.shared.data.SubmissionRequest;
import pl.gda.pg.shared.data.SubmissionResponse;

import javax.inject.Inject;
import javax.inject.Named;
import java.nio.file.Path;

import static pl.gda.pg.shared.services.VertxServices.DATABASE_SUBMISSION_UPDATE;

@Named
public class ProcessRunner {

	@Inject
	private ProjectFilesCreator structureCreator;
	@Inject
	private GradleRunner runner;
	@Inject
	private ResponseBuilder responseBuilder;
	@Inject
	private VertxWrapper vertxWrapper;

	public void runProcess(SubmissionRequest files) {
		vertxWrapper.vertx().executeBlocking(createProcess(files), this::onComplete);
	}

	private void onComplete(AsyncResult<Integer> tAsyncResult) {
		SubmissionResponse response = responseBuilder.build(tAsyncResult.result());
		vertxWrapper.eventBusWrapper().send(DATABASE_SUBMISSION_UPDATE, response);
	}

	private Handler<Future<Integer>> createProcess(SubmissionRequest files) {
		return future -> {
			Path baseDir = structureCreator.create(files);
			runner.runGradle(baseDir.toString());
			future.complete(files.getId());
		};
	}
}
