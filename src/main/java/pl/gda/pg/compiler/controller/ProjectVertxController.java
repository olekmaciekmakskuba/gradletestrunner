package pl.gda.pg.compiler.controller;

import pl.gda.pg.commons.vertx.VertxWrapper;
import pl.gda.pg.shared.data.SubmissionRequest;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import static pl.gda.pg.shared.services.VertxServices.COMPILER;

@Named
public class ProjectVertxController {

	@Inject
	private ProcessRunner runner;
	@Inject
	private VertxWrapper vertx;

	@PostConstruct
	public void start() {
		vertx.eventBusWrapper().consumer(COMPILER, runner::runProcess, SubmissionRequest.class);
	}
}
