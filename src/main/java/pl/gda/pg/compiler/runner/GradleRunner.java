package pl.gda.pg.compiler.runner;

import javax.inject.Named;
import java.io.IOException;

@Named
public class GradleRunner {

	public void runGradle(String gradleDir) {
		String command = "cmd /c cd \"" + gradleDir + "\" & gradle test";
		Process process;
		try {
			process = Runtime.getRuntime().exec(command);
			process.waitFor();
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}
}
