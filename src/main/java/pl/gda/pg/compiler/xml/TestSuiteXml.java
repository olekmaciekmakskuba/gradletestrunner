package pl.gda.pg.compiler.xml;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "testsuite")
@XmlSeeAlso(TestCaseXml.class)
public class TestSuiteXml {

	private List<TestCaseXml> testCases = new ArrayList<>();

	@XmlElement(name = "testcase")
	public List<TestCaseXml> getTestCases() {
		return testCases;
	}

	public void setTestCases(List<TestCaseXml> testCases) {
		this.testCases = testCases;
	}
}
