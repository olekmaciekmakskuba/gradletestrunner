package pl.gda.pg.compiler.xml;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

@XmlRootElement(name = "testcase")
@XmlSeeAlso(FailureXml.class)
public class TestCaseXml {

	private String name;
	private FailureXml failure;

	@XmlElement
	public FailureXml getFailure() {
		return failure;
	}

	public void setFailure(FailureXml failure) {
		this.failure = failure;
	}

	@XmlAttribute
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isPassed() {
		return failure == null;
	}

	public String getMessage() {
		return isPassed() ? "" : failure.getMessage();
	}
}
