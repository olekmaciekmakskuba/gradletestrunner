package pl.gda.pg.compiler.xml;

import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.transform.stream.StreamSource;
import java.nio.file.Path;

@Named
public class Parser {

	@Inject
	private Jaxb2Marshaller jaxb;

	public TestSuiteXml parse(Path filePath) {
		return (TestSuiteXml) jaxb.unmarshal(new StreamSource(filePath.toString()));
	}
}
