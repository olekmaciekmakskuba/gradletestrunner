package pl.gda.pg.compiler.structure;

import org.apache.commons.io.FileUtils;
import pl.gda.pg.shared.data.File;
import pl.gda.pg.shared.data.SubmissionRequest;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Base64;

@Named
public class ProjectFilesCreator {

	@Inject
	private ProjectPaths paths;

	public Path create(SubmissionRequest project) {
		Path basePath = paths.getProjectWorkspace(project.getId());
		project.getFiles().forEach(file -> processFile(file, basePath));

		return basePath;
	}

	private void processFile(File file, Path dir) {
		try {
			String folderName = file.getFolder();
			Path absolutePath = dir.resolve(folderName);
			Path pathToFile = createFile(absolutePath, file.getName());

			byte[] decodedFile = Base64.getDecoder().decode(file.getContent());
			writeToFile(decodedFile, pathToFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void writeToFile(byte[] fileContent, Path pathToFile) throws IOException {
		FileUtils.writeByteArrayToFile(pathToFile.toFile(), fileContent);
	}

	private Path createFile(Path directory, String fileName) throws IOException {
		Files.createDirectories(directory);
		Path filePath = directory.resolve(fileName);
		return Files.createFile(filePath);
	}
}
