package pl.gda.pg.compiler.structure;

import javax.inject.Named;
import java.nio.file.Path;
import java.nio.file.Paths;

@Named
public class ProjectPaths {
	private final Path root = Paths.get(System.getProperty("user.dir"));
	private final Path workspace = root.resolve("workspace");

	public Path getProjectWorkspace(Integer projectId) {
		return workspace.resolve(projectId.toString());
	}

	public Path getProjectTests(Integer projectId) {
		return workspace.resolve(projectId.toString()).resolve("build").resolve("test-results");
	}
}
